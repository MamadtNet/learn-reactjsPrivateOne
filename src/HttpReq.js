import React from "react"
import Employee from "./Employee"
import EmployeeForm from "./EmployeeForm";



class HttpReq extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <>
                <hr/>
                <h1 style={{textAlign : "center", color : "red"}}>Http Request</h1>
                <Employee/>
                <hr/>
                <EmployeeForm/>
            </>
        )
    }
}

export default HttpReq