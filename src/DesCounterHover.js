import React from "react";
import desCounter from "./desCounter";

class DesCounterHover extends React.Component {

    render() {
        return (
            <div>
                <h2 onMouseOver={this.props.decrement}>{this.props.counter}</h2>
                <p>props in index myName {this.props.myName}</p>
            </div>
        );
    }

}

export default desCounter(DesCounterHover, 10)