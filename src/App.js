import React from "react";
import Car from "./Car";


class App extends React.Component { //for function set arg = props
    constructor(props) {
        super(props)
        this.state = {
            color: "Yellow",
            name: props.name //don't set it here
        }//variable that you can use for your class -> when sate change, re-render !
    }

    //onClick event func
    shoot = (a, event) => {
        console.log(event)
        if (event.type === 'click')
            this.setState({color: a})
    }

    render() { // render return what i want
        console.log(this.props)
        const carDetail = {model: 'Benz', color: "Black"}
        return (
            <React.Fragment>
                <h1>Welcome to my website {this.state.name}</h1>
                <button onClick = {(event) => this.shoot("Black", event)}>Shoot</button>
                <p>the state color is {this.state.color}</p>
                <p>this is paragraph props.color : {this.props.color}</p>
                <Car Appstate={this.state} carDetail={carDetail} name="name_props"/>{/*component in component*/}
            </React.Fragment>
        )
    }

    // Note: React Props are read-only!
    // You will get an error if you try to change their value
}

export default App

/*
components has a lifecycle ->
phases of component :
1) {Mounting : when component get started}

2) {Updating : when you update variables}

3) {Unmounting : when component's work is Ended }
*/
//------------------'Mounting'-------------------------------------------------------
/*
(Mounting means putting element into DOM.) :
1) constructor() 2) getDerivedStateFromProps() 3) render() 4) componentDidMount()
   *render method is required and will always  be called

2) getDerivedStateFromProps() :
   *called before render()
   *This is the natural place to set the state object based on the initial props.
   *It takes state as an argument, and returns an object with changes to the state

3) componentDidMount() :
   *called after render()
   *check out 'Header.js'
   */
//------------------'Updating'-------------------------------------------------------
/*
if there is a change in the component's state or props -> component Updated
has 5 method ->
1) getDerivedStateFromProps() :
    *also we have this ! first method that component updated
    *also this is the natural place to set the state
    *see Header.js
2) shouldComponentUpdate()
    *you can return boolean value that React should continue with the rendering !!
    * default : true
    *see Header.js
3) render() -> of course !
4) getSnapshotBeforeUpdate(prevProps, prevState)
    *After Update -> you can see props and state (snapshot)
    *You must call componentDidUpdate() for using getSnapshotBeforeUpdate
    *see UpdateExample.js
5) componentDidUpdate()
    *call after the component is updated
    *see Header.js
    */
//------------------'Unmounting'-------------------------------------------------------
/*
when a component is removed from the DOM, or unmounting as React likes to call it
has 1 method
  componentWillUnmount()
    *method is called when the component is about to be removed from the DOM.
    *see Unmount.js
 */