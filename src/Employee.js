import React from "react"
import axios from "axios"


class Employee extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            employees : [],
            employees_axios : [],
            error : ''
        }

    }

    componentDidMount() {
        let url = "http://dummy.restapiexample.com/api/v1/employees" //Fake HttpReq
        fetch(url)
            .then(response => response.json())
            .then(data => {
                console.log(data)
                this.setState({
                    employees : data.data
                })
            }
            )
            .catch(error => console.log(error))
        axios.get(url)
            .then(response => {
                console.log(response)
                this.setState({
                    employee_axios : response.data
                })

            })
            .catch(err => {
                this.setState({
                    error : 'An error Occurred !'
                })
            })
    }

    render() {
        let data = this.state.employees.map((employee) => {
            return(<p key={employee.id}>{employee.employee_name}</p>)
        })
        const {employees, error} = this.state
        let data_axios = employees.length !== 0 ?
            employees.map((employee) => <p>{employee.employee_name}</p>) : <p>{error}</p>
        return (
            <div>
                Employee with fetch :
                {data}
                <hr/>
                Employee with axios :
                {data_axios}
            </div>
        )
    }
}

export default Employee