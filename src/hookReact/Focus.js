import React, {useEffect, useRef} from "react";

function Focus() {
    const inputRef = useRef(null)
    useEffect(() => {
        inputRef.current.focus()
    }, [])
    return(
        <>
            <br/>
            focus on this
            <input ref={inputRef}/>
        </>

    )

}

export default Focus