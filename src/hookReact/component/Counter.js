import React, {useState, useMemo} from "react";

function Counter(props) {
    const [counterOne, setCounterOne] = useState(0)
    const [counterTwo, setCounterTwo] = useState(10)

    const handleCounterOne = () => {
        setCounterOne(counterOne + 1)

    }
    const handleCounterTwo = () => {
        setCounterTwo(counterTwo + 1)
    }
    const even = useMemo(() => {
        let i = 0
        while (i < 2000000000){
            i++
        }
        if(counterOne % 2 === 0){
            return 'even'
        }
        else {
            return 'odd'
        }
    }, [counterOne])
    return (
        <>
            <p>counter 1 : {counterOne}  {even}</p>
            <p>counter 2 : {counterTwo}</p>
            <button onClick={handleCounterOne}>Increment 1</button>
            <button onClick={handleCounterTwo}>Decrement 1</button>
        </>
    )
}

export default Counter