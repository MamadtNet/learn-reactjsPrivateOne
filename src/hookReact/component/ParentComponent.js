import React, {useState, useCallback} from "react";
import Name from "./Name";
import Title from "./Title";
import ButtonName from "./ButtonName";
import Family from "./Family";
import ButtonFamily from "./ButtonFamily";

function ParentComponent() {
    const [name, setName] = useState('Hasan')
    const [family, setFamily] = useState('Kalantari')

    const handleName = useCallback (()=> {
        setName('Mohammad')
    }, [name])

    const handleFamily = useCallback (() => {
        setFamily('Tavallai')
    }, [family])

    return (
        <>
            <Title/>
            <Name name={name}/>
            <ButtonName nameHandler={handleName}>Change Name</ButtonName>
            <Family name={family}/>
            <ButtonFamily familyHandler={handleFamily}>Change Family</ButtonFamily>
        </>
    )
}

export default ParentComponent