import React from "react";

function ButtonFamily(props) {
    console.log('ButtonFamily rendered')
    return (
        <>
            <button onClick={props.familyHandler}>
                {props.children}
            </button>
        </>
    )
}

export default React.memo(ButtonFamily)