import {useEffect} from "react";

function useDocTitle(counter) {
    useEffect(() => {
        document.title = `counter : ${counter}`
    }, [counter])
}

export default useDocTitle