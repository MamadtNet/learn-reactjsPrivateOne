import React, {Component} from 'react'
import MouseComponent from "./MouseComponent";
import MouseHook from "./MouseHook";

class MouseContainer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            display: true
        }
    }

    render() {
        return (
            <>
                {this.state.display && <MouseHook/>}
                <button onClick={() => this.setState({display : false})}>Hide</button>
            </>
        )
    }
}

export default MouseContainer