import React, {useState} from "react";
import useDocTitle from "./hooks/useDocTitle";
function NewCounterHook() {
    const [counter, setCounter] = useState(0)
    useDocTitle(counter)
    return(
        <>
            <h1>counter : {counter}</h1>
            <button onClick={() => setCounter(counter + 1)}>Increment</button>
        </>
    )
}

export default NewCounterHook