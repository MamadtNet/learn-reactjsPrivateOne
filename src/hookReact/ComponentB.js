//Hook Context
import React, {useContext} from "react";
import ComponentC from "./ComponentC";
import {UserContext, SalaryContext} from "./App";

function ComponentB() {
    const name = useContext(UserContext)
    const salary = useContext(SalaryContext)
    return (
        <>
            <ComponentC/>
            {/*<p>useContext Hook => Name : {name}, Salary : {salary}</p>*/}
        </>
    )

}

export default ComponentB