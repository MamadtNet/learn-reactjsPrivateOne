import React from "react";

const TestCom3 = (props) => { //pureComponent for functions with props (memo)
    console.log('Memo Component Rendered')
    return (
        <h1>Memo Component(func) props.name : {props.name}</h1>
    )
}
export default React.memo(TestCom3) // React.memo() is an Higher Order Component