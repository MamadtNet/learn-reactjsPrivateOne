import React, {Component} from 'react'
import {NavLink, BrowserRouter as Router, Link, Route, Switch} from 'react-router-dom'

import Test1 from "./Test1";
import Test2 from "./Test2"
import NotFound from "./NotFound";
import Test3 from "./Test3";

class Routing extends Component {
    render() {
        const user = ({match}) => <p>*User {match.params.id} </p>

        return (
            <React.Fragment>
                <hr/>
                <h1>Routing</h1>
                <Router>
                    <ul>
                        <li>
                            <Link to={"/"}>Home Page</Link>
                        </li>
                        <li>
                            <NavLink activeClassName = {"classActive"} to={"/users"}>Users</NavLink>
                        </li>
                        <li>
                            <Link to={"/users"}>User 1</Link>
                        </li>
                        <li>
                            <Link to={"/users/1"}>user 1</Link>
                        </li>
                        <li>
                            <Link to={"/users/2"}>User 2</Link>
                        </li>
                        <li>
                            <Link to={"/users/3"}>user 3</Link>
                        </li>
                    </ul>
                    <Switch>
                        <Route exact path={"/"} component={Test1}/>
                        <Route exact path={"/users"} component={Test2}/>
                        <Route path={"/users/:id"} component={Test3}/>
                        {/*<Route path={"/users/:id"} component={user}/>*/}

                        <Route component={NotFound}/>
                    </Switch>
                </Router>
            </React.Fragment>
        )
    }
}

export default Routing