import React from "react";
import DesCounterHover from "./DesCounterHover";
import desCounter from "./desCounter";

class DesCounterClick extends React.Component {
    render() {
        return (
            <div>
                <hr/>
                <h1 style={{textAlign: "center", color: "red"}}>Higher Order Components(HOC)</h1>
                <button onClick={this.props.decrement}>Click : {this.props.counter}</button>
                <p>props in index myName {this.props.myName}</p>
                <DesCounterHover myName={"Mohammad 2"}/>
            </div>
        );
    }

}

export default desCounter(DesCounterClick, 50) // HOC -> const hocName(OriginalComponent) => {}