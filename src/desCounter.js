//HOC
import React from "react";

const desCounter = (WrappedComponent, number) => {
    class DesCounter extends React.Component {
        constructor(props) {
            super(props);
            this.state = {
                counter: 1000
            }
        }

        desHandler = () => {
            this.setState((prevState) => {
                return {
                    counter: prevState.counter - number
                }
            })

        }

        render() {
            return (
                <WrappedComponent
                    counter={this.state.counter}
                    decrement={this.desHandler}
                    {... this.props}/> // you can set props from index.js like that
            )
        }

    }

    return DesCounter
}

export default desCounter