import React from "react";

const scales = {
    C: "Celsius",
    F: "Fahrenheit"
}

class TemperatureInput extends React.Component {

    handleChange = (event) => {
        this.props.handleChange(event.target.value)
    }

    render() {
        return (
            <div>
                <label>{scales[this.props.type]}</label>
                <input
                    type={"text"}
                    value={this.props.temperature}
                    onChange={this.handleChange}/>
                <br/>
            </div>
        )
    }
}

export default TemperatureInput