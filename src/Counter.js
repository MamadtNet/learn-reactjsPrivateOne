import React from "react";

class Counter extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            counter : 100
        }
    }
    clickHandler = () => {
        this.setState((prevState)=>{
            return({
                counter : prevState.counter - 1
            })
        })
    }
    render() {
        return(
            <div>
                {this.props.render(this.clickHandler, this.state.counter)}
            </div>
        )
    }
}

export default Counter