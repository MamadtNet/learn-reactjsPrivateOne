import React from "react";
import TemperatureInput from "./TemperatureInput";
import * as myFunc from "./TryConvert"

class LiftingStateUp extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            scale : 'C',
            temp : ''
        }

    }

    handleChangeCelsius = (temp) => {
        this.setState({
            scale : 'C',
            temp : temp
        })
    }

    handleChangeFahrenheit = (temp) => {
        this.setState({
            scale : 'F',
            temp : temp
        })
    }




    render() {
        const celsius = this.state.scale === 'F' ?
            myFunc.tryConvert(this.state.temp, myFunc.toCelsius) : this.state.temp
        const fahrenheit = this.state.scale === 'C' ?
            myFunc.tryConvert(this.state.temp, myFunc.toFahrenheit) : this.state.temp

        return (
            <React.Fragment>
                <h1>Lifting State Up</h1>
                <TemperatureInput temperature = {celsius} type = "C" handleChange = {this.handleChangeCelsius}/>
                <TemperatureInput temperature = {fahrenheit} type = "F" handleChange = {this.handleChangeFahrenheit}/>
            </React.Fragment>
        )
    }
}


export default LiftingStateUp