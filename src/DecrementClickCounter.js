import React from "react";

class DecrementClickCounter extends React.Component{


    render() {
        return(
            <button onClick={this.props.clickHandler}>Click : {this.props.counter}</button>
        )
    }

}

export default DecrementClickCounter