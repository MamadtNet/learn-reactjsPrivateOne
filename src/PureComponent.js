import React from "react";
import TestCom1 from "./TestCom1";
import TestCom2 from "./TestCom2";
import TestCom3 from "./TestCom3";

class PureComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: "mohammad"
        }
    }

    componentDidMount() {
        setInterval(() => {
            this.setState({
                name: "Hasan"
            })
        }, 2000)
    }


    render() {
        console.log('Main Component Rendered')
        return (
            <>
                <h2>Main Component</h2>
                <TestCom1/>
                <TestCom2/>
                <TestCom3 name = {this.state.name}/>
            </>
        )
    }
}

export default PureComponent