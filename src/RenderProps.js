import React from "react";
import User from "./User";
import DecrementHoverCounter from "./DecrementHoverCounter";
import Counter from "./Counter";
import DecrementClickCounter from "./DecrementClickCounter";
import PortalDemo from "./PortalDemo";

class RenderProps extends React.Component {
    render() {
        return (
            <>
                <hr/>
                <h1 style={{textAlign: "center", color: "red"}}>Render Props - Portal</h1>
                <User name={(isLoggedIn) => isLoggedIn ? "Mohammad Tavallai" : "Guest"}/>
                <Counter render={(decrementEvent, counter) =>
                    <DecrementHoverCounter clickHandler={decrementEvent} counter={counter}/>}/>

                <Counter render={(decrementEvent, counter) =>
                    <DecrementClickCounter clickHandler={decrementEvent} counter={counter}/>}/>
                {/*<Counter>*/}
                {/*    {(decrementEvent, counter) =>*/}
                {/*        <DecrementClickCounter clickHandler={decrementEvent} counter={counter}/>}/>*/}
                {/*    /!*{(decrementEvent, counter) =>*!/*/}
                {/*    /!*    <DecrementHoverCounter clickHandler={decrementEvent} counter={counter}/>}/>*!/*/}
                {/*</Counter>*/}
                {/*<DecrementClickCounter/>*/}
                {/*<DecrementHoverCounter/>*/}
                <h1>Portals</h1>
                <PortalDemo/>
            </>
        )
    }

}

export default RenderProps