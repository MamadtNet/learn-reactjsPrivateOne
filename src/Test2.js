import React from "react";
import Test3 from "./Test3";
import UserContext from "./UserContext";

class Test2 extends React.Component {
    static contextType = UserContext // Just in class use

    render() {
        return (
            <div>
                <h1>Users</h1>
                <h5>Written in Test2.js</h5>
                {this.context}
                <Test3/>
            </div>
        )
    }

}

export default Test2