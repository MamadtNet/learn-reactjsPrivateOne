import React from "react";
import {UserConsumer} from "./UserContext";

class Test3 extends React.Component {
    // eslint-disable-next-line no-useless-constructor
    constructor(props) {
        super(props);
    }

    render() {
        console.table(this.props)
        return (
            <div>
                {/*<h1>user {this.props.match.params.id}</h1>*/}
                {/*<button onClick={this.handleClick} type={"button"}>Go to users</button>*/}
                <h5>Written in Test3.js</h5>
                <UserConsumer>
                    {
                        (userName) => {
                            return <p>Your name is {userName}</p>
                        }
                    }
                </UserConsumer>
            </div>
        )
    }

    handleClick = () => {
        console.table(this.props)
        this.props.history.push("/users")
    }
}

export default Test3