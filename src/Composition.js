import React from "react";

function FancyBorder(props) {
    return (
        <React.Fragment>
            <div className={'FancyBorder FancyBorder-' + props.color}>
                {props.children}
            </div>
        </React.Fragment>

    );
}

function SplitPane(props) {
    return (
        <div className="SplitPane">
            <div className="SplitPane-left">
                {props.left}
            </div>
            <div className="SplitPane-right">
                {props.right}
            </div>
        </div>
    );
}

function App() {
    return (
        <SplitPane
            left={
                <Contacts/>
            }
            right={
                <Chat/>
            }/>
    );
}

function Contacts() {
    return (
        <p>This is Contacts Object in "props.left" that called in SplitPane.</p>
    )

}

function Chat() {
    return (
        <p>This is Chat Object in "props.right" that called in SplitPane.</p>
    )

}

function Dialog(props) {
    return (
        <FancyBorder color="blue">
            <h1 className="Dialog-title">
                {props.title}
            </h1>
            <p className="Dialog-message">
                {props.message}
            </p>
            <p className="Dialog-purpose">
                {props.purpose}
            </p>
        </FancyBorder>
    );
}

function WelcomeDialog() {
    return (
        <FancyBorder color="blue">
            {/* Example 1 */}
            <h1 className="Dialog-title">
                Welcome to Compositions
            </h1>
            <p className="Dialog-message">
                Thank you for visiting our spacecraft!
            </p>
            {/* Example 2 */}
            <hr/>
            <App/>
            {/* Example 3 */}
            <hr/>
            <Dialog
                title="Welcome to Specialization"
                message="Thank you for visiting our spacecraft!"
                purpose="WelcomeDialog is a special case of Dialog"/>
            {/* WelcomeDialog is a special case of Dialog
                you can use it in class too !
            */}

        </FancyBorder>
    );
}

export default WelcomeDialog