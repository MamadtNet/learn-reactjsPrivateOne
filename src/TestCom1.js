import React from "react";

class TestCom1 extends React.PureComponent {
    // pure Component when rendered that props and state must change in shallow
    // means state and props changed in shallow Comparison("mohammad" to "Ali")
    // shouldComponentUpdate if true render updated -> in Shallow
    // in Regular Component always shouldComponentUpdate == true
    // for Array you must changed References
    //With Pure Components you can improve performances
    render() {
        console.log('pure Component Rendered')//one time Rendered
        return (
            <div>
                <h3>Pure Component</h3>
            </div>
        )
    }
}

export default TestCom1