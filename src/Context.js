import React from "react";
import Test1 from "./Test1";
import {UserProvider} from "./UserContext";
import ErrorBoundary from "./ErrorBoundary";

class Context extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <React.Fragment>
                <hr/>
                <h1 style={{textAlign: "center"}}>Context - Error Boundaries</h1>
                <UserProvider value={"Tavallaee"}>
                    <Test1/>
                </UserProvider>
                <div>
                    <ErrorBoundary>
                    <Test1 myName = "Mamad in Error Buondry"/>
                    </ErrorBoundary>
                    <ErrorBoundary>
                    <Test1 myName = "Ali"/>
                    </ErrorBoundary>

                </div>
            </React.Fragment>

        )
    }

}

export default Context