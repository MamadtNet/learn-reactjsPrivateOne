import React from "react";
import styles from './style.module.css'

class Header extends React.Component {
    constructor(props) {
        super(props);
        this.state = {favoritecolor: "red"};
    }

    shouldComponentUpdate() {
        return false;
    }

    static getDerivedStateFromProps(props, state) {
        return {favoritecolor: props.favcol};
    }

    changeColor = () => { //after Button work getDerivedStateFromProps set favoritecolor yellow
        this.setState({favoritecolor: "blue"});
    }

    // componentDidMount() {
    // setTimeout(() => {
    //     this.setState({favoritecolor: "yellow"})
    // }, 1000)
    // }

    render() {
        return (
            <div>
                <h1>My Favorite Color is {this.state.favoritecolor}</h1>
                <button className={styles.bigblue} type="button" onClick={this.changeColor}>Change color</button>
                <p className = "mydiv" >Use className for style</p>
            </div>
        );
    }
}

export default Header